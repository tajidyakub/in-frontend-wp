/**
 * The configuration is used mainly to deploy your
 * plugin via grunt-rscync.
 */
module.exports = {
  slug: 'plugin-slug',
  version: '1.0.0',
  git: 'https://bitbucket.org/tajidyakub/plugin-slug-wp.git',
  sync: {
    localPath: 'src/plugins/',
    remotePath: 'web:/var/www/domain.tlf/htdocs/wp-content/plugins/plugin-slug',
    exclude: []
  }
}
