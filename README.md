# Inputs Frontend WordPress Plugin

> Seamlessy bridge your remote forms and manage inputs from WordPress.

Item          | Desc
------------- | -------------
version       | 1.0.0
documentation | https://bitbucket.org/tajidyakub/in-frontend-wp/wiki
repository    | https://bitbucket.org/tajidyakub/in-frontend-wp.git
issues        |https://bitbucket.org/tajidyakub/in-frontend-wp/issues
maintainer    |Tajid Yakub <tajid.yakub@gmail.com>


![](src/plugins/assets/imgs/inputs-frontend.png)

## Screenshots

> Coming up.

## Released

### Version 1.0.0

- Includes ACF extension inside modules
- Dedicated Setting Page to Setup the remote form
- Data and input status view page

## Stacks

Build on top of, to work with or extendable through one of these stacks;

- PHP
- MySQL
- WordPress Plugin Actions and Hooks
- Guzzle
- Node.js
- Advanced Custom Field WordPress Plugin
