#!/usr/bin/env node

/**
 * Execute this script to adjust the boilerplate
 * with your slugs and prefixes provided.
 */

const app = require('commander');

app
  .version('0.1.0', '-v, --version')
  .description('opinionated Boilerplate of your WordPress plugin development.')
  .usage('[option]')
  .option('-d, --defaults', 'Write default configurations')
  .action(function(cmd) {

    })
  .parse(process.argv)
