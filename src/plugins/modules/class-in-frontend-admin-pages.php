<?php

class InFrontendAdminPages
{
    protected $uri;

    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    public static function add($uri)
    {
        acf_add_options_sub_page(array(
            'page_title' 	=> 'Inputs Frontend Settings',
            'menu_title'	=> 'Inputs Frontend',
            'parent_slug'	=> 'options-general.php',
        ));
    }

    public static function add_admin_page()
    {
        acf_add_options_page(
            array(
                'page_title' => 'Remote Forms',
                'menu_title' => 'Remote Forms',
                'menu_slug' => 'remote_forms',
                'icon_url' => $this->uri . 'assets/imgs/remote-forms-20x20.png',
                'location' => '2'
            )
        );
        acf_add_options_sub_page(
            array(
                'menu_title' => 'Posts Viewer',
                'page_title' => 'Posts',
                'parent_slug' => 'remote_forms'
            )
        );
        acf_add_options_sub_page(
            array(
                'menu_title' => 'Settings',
                'page_title' => 'Remote Form Settings',
                'parent_slug' => 'remote_forms'
            )
        );
    }

    private function set_admin_menu($menu)
    {

    }
}
