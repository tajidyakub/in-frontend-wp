<?php

/**
 * Core plugin class.
 *
 * @link       https://tajidyakub.com/
 * @since      1.0.0
 *
 * @package    InFrontend
 * @subpackage InFrontend/modules
 */

/**
 * Core plugin class.
 *
 * This is used to define internationalization, plugin hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    InFrontend
 * @subpackage InFrontend/modules
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */
class InFrontend {
    protected $slug;
    protected $version;
    protected $plugin_uri;
    protected $plugin_path;
    protected $loader;

    /**
     * Class construct.
     *
     * Populate global plugin's properties.
     *
     * @since 1.0.0
     * @param array $options Array of plugin's global properties an Class options
     * @return void
     *
     * $args = array(
     *  'slug' => 'plugin slug',
     *  'version' => 'plugin version',
     *  'uri' => 'plugin url',
     *  'path' => 'plugin file system path',
     * );
     *
     */
    public function __construct($options) {
        // Populate properties
        $this->slug        = $options['slug'];
        $this->version     = $options['version'];
        $this->plugin_uri  = $options['uri'];
        $this->plugin_path = $options['path'];
        $this->requires();      // Require Class Files.
        $this->set_locale();    // Internazionalization
        $this->load_assets();   // Enqueue Scripts and Styles
        $this->init_acf();      // Include's ACF files
        $this->admin_pages();   // Add admin pages
        $this->init_form();     // Init the form using Shortcode API
    }

    /**
     * Requires module files.
     *
     * Initialize the plugin's loader object
     * to register hooks.
     *
     * @access private
     * @since  1.0.0
     * @return void
     */
    private function requires() {
        require_once $this->plugin_path . 'modules/class-in-frontend-assets.php';
        require_once $this->plugin_path . 'modules/class-in-frontend-i18n.php';
        require_once $this->plugin_path . 'modules/class-in-frontend-acf.php';
        require_once $this->plugin_path . 'modules/class-in-frontend-admin-pages.php';
        require_once $this->plugin_path . 'modules/class-in-frontend-form.php';
        require_once $this->plugin_path . 'modules/class-in-frontend-loader.php';
        $this->loader = new InFrontendLoader();     // Hooks and Filters loader
    }
    /**
     * Plugin's internatioalization.
     *
     * @access private
     * @since  1.0.0
     * @return void
     */
    private function set_locale() {
        $i18n = new InFrontendI18n();
        $this->loader->add_action( 'plugins_loaded', $i18n, 'load_plugin_textdomain' );
    }

    private function load_assets() {
        $assets_loader = new InFrontendAssets($this->plugin_path,$this->plugin_uri);
        $this->loader->add_action('wp_enqueue_scripts', $assets_loader, 'enqueues');
    }

    private function init_acf() {
        $acf = new InFrontendACF($this->plugin_path, $this->plugin_uri);
        $this->loader->add_filter('acf/settings/path', $acf, 'acf_path');
        $this->loader->add_filter('acf/settings/dir', $acf, 'acf_uri');
        $this->loader->add_filter('acf/settings/load_json', $acf, 'load_json_point');
        $this->loader->add_filter('acf/settings/save_json', $acf, 'save_json_point');
        require_once $this->plugin_path . 'modules/acf/acf.php';
    }

    private function admin_pages()
    {
        InFrontendAdminPages::add($this->plugin_uri);
    }

    private function init_form()
    {
        $remote_forms = get_field('forms', 'option');
        $rform        = new InFrontendForm($remote_forms);
        $this->loader->add_action('init', $rform, 'set_form_shortcode');
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }
}
