<?php
class InFrontendI18n {

    public function load_plugin_textdomain() {
        load_plugin_textdomain(
            'in-frontend',
            false,
            dirname( dirname( plugin_basename( __FILE__ ) ) ) . 'languages/'
        );
    }

}
