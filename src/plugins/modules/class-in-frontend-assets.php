<?php

/**
 * Define Plugin Asset files includer class.
 *
 * @link       https://tajidyakub.com/
 * @since      1.0.0
 *
 * @package    InFrontend
 * @subpackage InFrontend/modules
 */

/**
 * Asset Files Includer class.
 *
 * Enqueue .js and .css inside respective directories.
 *
 * @since      1.0.0
 * @package    InFrontend
 * @subpackage InFrontend/modules
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */

class InFrontendAssets {
    protected $path;
    protected $uri;
    public function __construct($path,$uri) {
        $this->path = $path;
        $this->uri  = $uri;
    }

    public function enqueues() {
        $assets     = $this->get_asset_files();
        $styles     = $assets['styles'];
        $scripts    = $assets['scripts'];

        if (!empty($styles)) {
            foreach ($styles as $style) {
                $args = $this->set_asset_args($style);
                wp_enqueue_style($args['handle'], $args['src'], $args['deps'], $args['version'], 'all');
            }
        }
        if (!empty($scripts)) {
            foreach ($scripts as $script) {
                $args = $this->set_asset_args($script);
                wp_enqueue_script($args['handle'], $args['src'], $args['deps'], $args['version'], true);
            }
        }
    }

    private function set_asset_args($filename) {
        $exploded   = explode('.', $filename);
        $type       = $exploded[count($exploded)-1];
        $slug       = $exploded[0];
        $name       = $exploded[1];
        $src        = $this->uri . 'assets/' . $type . '/' . $filename;
        $path       = $this->path . 'assets/' . $type . '/' . $filename;
        $version    = filemtime($path);
        $deps       = array();

        // handle 'slug/css/name'
        $handle     = $slug . '/' . $type . '/' . $name;
        return array(
            'handle' => $handle,
            'src' => $src,
            'deps' => $deps,
            'version' => $version
        );
    }
    private function get_asset_files() {
        // get file names in assets path
        $style_files  = scandir($this->path . 'assets/css/');
        $script_files = scandir($this->path . 'assets/js/');
        // filter the array
        if (count($style_files)>2) {
            $styles = array_filter($style_files, array($this, 'filter_css'));
        } else {
            $styles = array();
        }

        if (count($script_files)>2) {
            $scripts = array_filter($script_files, array($this, 'filter_js'));
        } else {
            $scripts = array();
        }

        return array(
            'styles' => $styles,
            'scripts' => $scripts
        );
    }

    private function filter_css($arr) {
        $exploded = explode('.', $arr);
        return in_array('css', $exploded);
    }

    private function filter_js($arr) {
        $exploded = explode('.', $arr);
        return in_array('js', $exploded);
    }
}
