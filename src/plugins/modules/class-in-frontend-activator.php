<?php

/**
 * Define Plugin Activator class.
 *
 * @link       https://tajidyakub.com/
 * @since      1.0.0
 *
 * @package    InFrontend
 * @subpackage InFrontend/modules
 */

/**
 * Plugin Activatoe class.
 *
 * Registered in activation and deactivation hooks.
 * Contains methods to be executed during plugin activation and deactivation.
 *
 * @since      1.0.0
 * @package    InFrontend
 * @subpackage InFrontend/modules
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */

class InFrontendActivator {
    public static function activate() {
        // create database table
    }
    public static function deactivate() {
        // deactivate
    }
}
