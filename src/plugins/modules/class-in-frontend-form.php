<?php

class InFrontendForm
{
    protected $remote_forms;
    protected $schemas;

    public function __construct($remote_forms = [])
    {
        $this->remote_forms = $remote_forms;
        $this->schemas      = array();
        $this->create_schema();
    }

    public function set_form_shortcode()
    {
        add_shortcode('rform', array($this, 'cb_form_shortcode'));
    }

    public function cb_form_shortcode($atts=[])
    {
        $atts = shortcode_atts(array(
            'slug' => null
        ), $atts);

        return $this->get($atts['slug']);
    }

    private function get($slug)
    {
        return $this->create_form[$slug];
    }

    private function create_form($slug)
    {
        $attributes = $this->schemas[$slug]['attributes'];
        $fields     = $this->schemas[$slug]['fields'];
        $form .= '<form ';
        foreach ($attributes as $key=>$val) {
            $form .= "{$key}=\"{$val}\" ";
        }
        $form .= '>';
        $form .= '  <div class="container>"';
        foreach ($fields as $field) {
            $form .= '      <div class="row">';
            $form .= '          <div class="col">'.$field['label'].'</div>';
            $form .= '          <div class="col">'.$this->create_form_element($field).'</div>';
            $form .= '      </div>';
        }
        $form .= '  </div>"';
        $form .= '</form>';
        return $form;
    }

    private function create_form_element($field)
    {
        $element  = "<{$field['tag']} ";
        foreach ($field['attributes'] as $key=>$val) {
            $element .= "{$key}=\"{$val}\" ";
        }
        if ($field['tag'] == 'input') {
            $element .= '/>';
        } elseif ($field['tag'] == 'select') {
            $element .= '>';
            foreach ($field['options'] as $option) {
                $element .= '<option value="'.$option['value'].'">'.$option['text'].'</option>';
            }
            $element .= '</select>';
        } elseif ($field['tag'] == 'img') {
            $element .= '/>';
        } elseif ($field['tag'] == 'button') {
            $element .= '>' . $field['text'] . '</button>';
        } elseif ($field['tag'] == 'ul') {
            $element .= '>';
            foreach ($field['li'] as $li) {
                $element .= '<li>'. $li .'</li>';
            }
            $element .= '</ul>';
        }
        return $element;
    }

    private function create_schema()
    {
        foreach ($this->remote_forms as $form) {
            $title  = $form['title'];
            $url    = $form['form_url'];
            $json   = $form['json_url'];
            $slug   = $this->create_slug($title);
            $schema = $this->parse_json($json);

            $this->schemas[$slug] = array(
                'title' => $title,
                'form_url' => $url,
                'form' => array(
                    'attributes' => $schema['attributes'],
                    'fields' => $schema['fields'],
                )
            );
        }
    }

    private function parse_json($json_url)
    {
        $json  = file_get_contents($json_url);
        $array = json_decode($json, true);
        return $array;
    }

    private function create_slug($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }
}
