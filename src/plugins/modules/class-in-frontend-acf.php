<?php

class InFrontendACF
{
    protected $path;
    protected $uri;

    public function __construct($path, $uri)
    {
        $this->path = $path;
        $this->uri  = $uri;
    }

    public function acf_path()
    {
        // Return ACF Path
        return $this->path . 'modules/acf/';
    }

    public function acf_uri()
    {
        return $this->uri .'modules/acf/';
    }

    public function load_json_point()
    {
        return $this->path . 'field-groups/';
    }

    public function save_json_point()
    {
        $paths = [];
        // remove original path (optional)
        // unset($paths[0]);
        // append path
        $paths[] = $this->path . 'field-groups/';
        // return
        return $paths;
    }
}
