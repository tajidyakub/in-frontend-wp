<?php
/**
 * Plugin's entry point.
 *
 * @package in-frontend
 * @version 1.0.0
 * @link    https://bitbucket.org/tajidyakub/in-frontend-wp.git
 *
 * @in-frontend
 * Plugin Name:       Inputs Frontend
 * Plugin URI:        https://bitbucket.org/tajidyakub/in-frontend-wp.git
 * Description:       Seamlessy bridge your remote forms and manage inputs from WordPress.
 * Version:           1.0.0
 * Author:            Tajid Yakub
 * Author URI:        https://tajidyakub.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-slug
 * Domain Path:       /languages
 */

/**
 * Kill the script if called directly
 */
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Define constants.
 */
define('IN_FRONTEND_SLUG', 'in-frontend');
define('IN_FRONTEND_VERSION', '1.0.0');

// Require Activator class
require_once plugin_dir_path( __FILE__ ) . 'modules/class-in-frontend-activator.php';

function in_frontend_activate() {
    InFrontendActivator::activate();
}

function in_frontend_deactivate() {
    InFrontendActivator::deactivate();
}

// Register activation and deactivation hooks
register_activation_hook( __FILE__, 'in_frontend_activate' );
register_deactivation_hook( __FILE__, 'in_frontend_deactivate' );

// Require core class
require_once plugin_dir_path( __FILE__ ) . 'modules/class-in-frontend.php';

// Plugin Intance wrapper functions.
function in_frontend_execute() {
    // Options for Plugin instance initialization.
    $options     = array(
        'slug'       => IN_FRONTEND_SLUG,
        'version'    => IN_FRONTEND_VERSION,
        'uri'        => plugin_dir_url(__FILE__),
        'path'       => plugin_dir_path(__FILE__),
    );
    $in_frontend = new InFrontend($options);
    $in_frontend->run();
}

in_frontend_execute();
